# Manage GitLab Runners via Ansible

This is an [Ansible Role][role] for managing both the [GitLab Runner][docs] and
individual runner instances.

[role]: https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html
[docs]: https://docs.gitlab.com/runner/

## Synopsis

Below please find an example on how to include the role and configure the
individual GitLab runner instances in an [Ansible Playbook][book]:

    # https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html
    ---
    - hosts:
        "all"

      roles:

        - role:
            "gitlab/runner"

          gitlab_runners:

            # Define a GitLab runner instance
            - parameters:
                executor: "shell"
                name: "gitlab-runner-example"
                registration-token: "VAxVVxLxyfxsz-XXXXXX"
                url: "https://gitlab.com"
              flags:
                - "run-untagged"
                - "docker-privileged=true"
                - "docker-tlsverify=false"

            # Remove a GitLab runner instance
            - parameters:
                name: "obsolete-runner-example"
                url: "https://gitlab.com"
              state:
                "absent"

            # Create a protected runner
            - parameters:
                name: "protected-runner-example"
                registration-token: "VAxVVxLxyfxsz-XXXXXX"
                url: "https://gitlab.com"
              # Personal Access Token for gitlab.com
              personal_access_token: "aaa8DBbbbc13cccsXBdd"
              protected: true


Note that it is recommended to manage the `gitlab_runners` variable using any
of the available [Ansible Inventory][data] mechanisms, instead of hard-coding
the associated values, especially the `registration-token` and the
`personal-access-token`.

To get a full documentation of the module execute

    $ ANSIBLE_LIBRARY=runner/ ansible-doc -t module gitlab_runner

[book]: https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html
[data]: https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html

## Running tests

Running minimal tests (dry-run):

    $ cd runner/library ; tox

Running tests with actual (un)registration of a runner:

    $ cd runner/library ; GITLAB_REGISTRATION_TOKEN=VAxVVxLxyfxsz-XXXXXX tox
